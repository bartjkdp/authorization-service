# Authentication and authorization with Traefik and Django

![Diagram that shows the request flow](/uploads/360470e06ec9cb7800cf34b39dd99d08/image.png)

This is an example how to perform authentication and authorization with Traefik and Django using a [ForwardAuth](https://doc.traefik.io/traefik/middlewares/forwardauth/) middleware in order to protect a backend service.

- OpenID Connect Identity provider for authentication
- Django microservice that authorizes the request
- An admin panel to manage users

## Getting started

Start the stack with:

```bash
docker-compose up
```

This will start the following services:

- **Traefik**: the proxy that sits before the authorization service and protected service
- **Authorization service**: a small Django project that performs user management and authorization
- **Database**: a PostgreSQL database for storing the state of the authorization service
- **Dex**: A lightweight OpenID Connect Identity Provider
- **Whoami**: the service that is protected

Now you can reach the proxy at http://localhost:8000. It will return an unauthorized request message because you are not logged in yet.

Login through http://localhost:8000/oidc/authenticate. Use the following credentials:

- Username: admin@example.com
- Password: password

After login you will be redirected and see the response of the protected Whoami service.

## Admin panel

To access the admin panel we first need to grand admin rights to the newly created user. First login to the authorization service shell:

```bash
docker-compose exec authorization-service python manage.py shell
```

Now insert the following code snippet to grant admin privileged to the user admin@example.com:

```python
from core.models import User
user = User.objects.get(email="admin@example.com")
user.is_admin = True
user.save()
quit()
```

Finally visit http://localhost:8000/admin to see the admin panel.

## Create new dummy users

New dummy users can be created in the Identity Provider in the [staticPasswords section](https://gitlab.com/bartjkdp/authorization-service/-/blob/master/dex/dex.yaml#L22).

## Change authorization logic

Authorization logic can be changed in the [authorization view](https://gitlab.com/bartjkdp/authorization-service/-/blob/master/authorization-service/core/views.py#L6).
