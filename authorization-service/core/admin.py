from django.contrib import admin
from .models import User
from django.utils.translation import gettext_lazy as _


class UserAdmin(admin.ModelAdmin):
    def has_add_permission(self, request):
        return False

    ordering = ['full_name']
    list_display = ['full_name', 'email', 'is_admin', 'last_login']
    search_fields = ['full_name', 'email']
    readonly_fields = ['full_name', 'email', 'last_login']
    fieldsets = (
        (None, {'fields': ['full_name', 'email', 'last_login']}),
        (_('Permissions'), {'fields': ['is_active', 'is_admin']})
    )


admin.site.register(User, UserAdmin)
