from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render


def authorize(request):
    ip = request.headers.get('X-Forwarded-For')
    path = request.headers.get('X-Forwarded-Uri')

    print('Request user: {} IP: {} Path: {}'.format(request.user, ip, path))

    if request.user.is_authenticated:
        return HttpResponse(status=200)

    # Add additional authorization logic here

    return HttpResponse('Unauthorized request', status=401)
