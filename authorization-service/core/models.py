from django.utils.translation import gettext_lazy as _
from django.db import models
from django.contrib.auth.models import (
    BaseUserManager, AbstractBaseUser
)

MAX_URL_LENGTH = 2000


class UserManager(BaseUserManager):
    def create_user(self, full_name, email, password=None):
        if not full_name:
            raise ValueError('Users must have a full name')

        if not email:
            raise ValueError('Users must have an email address')

        user = self.model(
            full_name=full_name,
            email=self.normalize_email(email),
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, full_name, email, password=None):
        user = self.create_user(
            full_name=full_name,
            email=self.normalize_email(email),
        )

        user.set_password(password)
        user.is_admin = True
        user.save(using=self._db)
        return user


class User(AbstractBaseUser):
    full_name = models.CharField(max_length=255, db_index=True)
    email = models.EmailField(verbose_name='email address', unique=True)
    external_id = models.CharField(max_length=255, unique=True, null=True)

    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)

    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['full_name']

    class Meta:
        ordering = ['full_name']

    def __str__(self):
        return self.email

    def has_perm(self, perm, obj=None):
        # pylint: disable=unused-argument
        return True

    def has_module_perms(self, app_label):
        # pylint: disable=unused-argument
        return True

    def has_usable_password(self):
        return False

    @property
    def is_staff(self):
        return self.is_admin

