#!/usr/bin/env sh
set -e

# Collect static
python /app/manage.py collectstatic --noinput

# Run migrations
python /app/manage.py migrate

# Start Gunicorn processes
uwsgi --http :8000 --module authorization_service.wsgi --processes 4 --threads 2 --static-map /admin/static=/app/static
